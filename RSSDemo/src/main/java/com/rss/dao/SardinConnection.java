package com.rss.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.github.sardine.Sardine;
import com.github.sardine.SardineFactory;
@Component
public class SardinConnection {
	@Autowired
	 private Environment env;

	public Sardine ConnectToSardin() 
	{
		Sardine sardine = SardineFactory.begin(env.getProperty("webdav.username"), env.getProperty("webdav.password"));
		return sardine;
		
	}// end ConnectToSardin

}
