package com.rss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RssDemoApplication {

	public static void main(String[] args) {
		System.out.println("Inside");
		SpringApplication.run(RssDemoApplication.class, args);
	}

}
