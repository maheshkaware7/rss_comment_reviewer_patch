package com.rss.model;

import org.springframework.http.HttpStatus;

public class ExceptionResponse {

	private String errorMessage;
	private String requestedURI;
	private int Errorcode;
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getRequestedURI() {
		return requestedURI;
	}
	public void setRequestedURI(String requestedURI) {
		this.requestedURI = requestedURI;
	}
	public int getErrorcode() {
		return Errorcode;
	}
	public void setErrorcode(int value) {
		// TODO Auto-generated method stub
		Errorcode = value;
	}
	
	public void callerURL(final String requestedURI) {
		this.requestedURI = requestedURI;
	}
	
}
