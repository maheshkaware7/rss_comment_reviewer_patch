package com.rss.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.rss.model.ExceptionResponse;
@ControllerAdvice
public class ExceptionController {
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(value=DocumentnotfoundException.class)
	 public  ResponseEntity<com.rss.model.ExceptionResponse> handleInvalidUser(DocumentnotfoundException user,HttpServletRequest request)
	 {
		 //@ResponseBody ExceptionResponse
		    com.rss.model.ExceptionResponse error = new ExceptionResponse();
			error.setErrorMessage(user.getMessage());
			error.callerURL(request.getRequestURI());
			error.setErrorcode(HttpStatus.NOT_FOUND.value());			
			return new ResponseEntity<ExceptionResponse>(error,HttpStatus.NOT_FOUND);
	 }
	 

}
